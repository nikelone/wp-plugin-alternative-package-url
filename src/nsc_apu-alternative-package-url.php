<?php
/*
Plugin Name: Alternative Package URL
Description: Development Tool: change the url of the installation package, to test the upgrade process of your custom plugin.
Author: Nikel Schubert
Version: 0.1
Author URI: https://nikel.co/
Text Domain: nsc-apu-alternative-package-url
License:     GPLv3

Alternative Package URL is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Alternative Package URL is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Alternative Package URL. If not, see {License URI}.
 */

add_filter('pre_set_site_transient_update_plugins', function ($transient) {

    /*
    Configuration start
     */

    /*
    Folder name of your Plugin in wp-content/plugins folder.
     */
    $plugin_folder = "my_plugin_folder";

    /*
    Name of your plugin main php file.
     */
    $plugin_main_php = "my_plugin.php";

    /*
    Slug of your plugin
     */
    $plugin_slug = "my-plugin";

    /*
    new version you want to test. To be able to work this number should be higher, then the version you have currently installed.
     */
    $plugin_version = "2.2";

    /*
    The url to the package which shall be updated.
    It make sense to create this with CI/CD.
     */
    $url_to_new_plugin_zip = "https://mypage.com/ci-cd/myplugin-newest.zip";

    /*
    End configuration
     */

    $path_to_main_file = $plugin_folder . "/" . $plugin_main_php;

    if (isset($transient->no_update[$path_to_main_file])) {
        unset($transient->no_update[$path_to_main_file]);
    }

    $info = new \stdClass();
    $info->id = "w.org/plugins/" . $plugin_folder;
    $info->slug = $plugin_slug;
    $info->plugin = $path_to_main_file;
    $info->new_version = $plugin_version;
    $info->package = $url_to_new_plugin_zip;
    $info->url = "https://wordpress.org/plugins/" . $plugin_slug;
    $info->icons = array();
    $info->banners = array();
    $info->banners_rtl = array();

    $transient->response[$path_to_main_file] = $info;

    return $transient;
});

# Easily test your wordpress update process

This is a very simple plugin which helps you to test the update process of your wordpress plugin.

It is only meant for development purposes!

# Installation

Just upload the plugin file to your wordpress plugin directory and change the variables in configuration part.

For more informtion and how to automatically create the needed zip file visit: https://nikel.co/how-tos/worpress-plugin-dev-test-your-plugin-upgrade-process/
